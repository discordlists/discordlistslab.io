---
title: Friday Weekly
subtitle: Weekly Friday Post!
date: 2018-08-03
tags: ["fridays", "blog"]
author: apollo
---

## Announcements
* Backend development is going to be started very shortly.
* Our domain will also be bought shortly, we are not saying what it is so that it may not be bought by one of our readers.

### This week at DiscordLists
* We did a little bit of cleaning up on our main website, added a few featues, and made it look better overall.
* We got a new dev, make sure to welcome `DJ Electro#1677` to the team!
* We partnered with an **awesome** hosting company, [Endless Hosting](https://theendlessweb.com)

### Misc
Random Fact: The most popular code editor on the DiscordLists dev team is Visual Studio Code.

Random Fact #2: Everybody on the dev team is from different states.