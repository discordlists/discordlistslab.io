---
title: Getting back to work
subtitle: We haven't been very active for the past while, and we know it.
date: 2018-08-18
tags: ["progress","blog"]
author: Luke
---


# Getting back to work
We haven't been very active for the past while, and we know it.

We definitely didn't know we wouldn't have been finished now, but guess what we haven't, so heres why. School started, our time progress really slowed down once school started, and even before that work on the project had just slowed down.
But we made a goal to start work on october 1st, and we're trying to that. Also we're working on some new fancy staff, Check out what to expect below.

## Making it fancier

- A new fancier website frontend, we're switching to [Bulma](https://bulma.io) for frontend.
- An actually *functional* website, that's right! We're going to make it work!
- A revamp to the discord bot, when work on the site slowed, so did the bot, so we're going to bring it back better than ever!

Well that's all i have to said, so bye i guess? - Luke