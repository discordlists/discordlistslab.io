---
title: First post!
subtitle: Welcome to our blog!
date: 2018-07-30
tags: ["blog"]
author: apollo
---

## What exactly is this?
This is our official blog where you can get updates on the website, the backend, and of course, the user base.

## What will be happening here?
Every week on **Fridays** we will post a new update, and if we deem somebody worthy, they might be featured in that post.

## What if I have an idea?
If you have an idea, send us an email over at [mail@example.com](#) and we'll get back to you as soon as possible.
