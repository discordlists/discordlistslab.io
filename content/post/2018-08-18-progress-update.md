---
title: Progress Update
subtitle: Update on DiscordLists Progress!
date: 2018-08-18
tags: ["progress_update","progress","blog"]
author: BluLightShow
---

# Progress Update
We know it's been quite a while so we'd like to update you guys on what we've been working on in the past couple of weeks to get DiscordLists closer to rolling out! 

## We've got a secreted hideout on the web now! (A domain!)
We've finally managed to get our domain setup for you guys to use! You can now visit [http://blog.discordlists.xyz](http://blog.discordlists.xyz) to get to our blog posts! Along with setting up our blog subdomain, we've also got our main domain setup aswell! To take a look at our progress on the main website, you can head over to [
http://discordlists.xyz](http://discordlists.xyz) and be amazed!

##  Much organization. Very important.
We've done some cleaning up behind the scenes. (don't worry, we used a vacuum, not a [*BROOM*](https://www.youtube.com/watch?v=sSPIMgtcQnU)) Things are now exactly 132% more organized and we should be able to progress much faster now. Very cool wumpus. Very cool.

## A discord bot that turns on. *NANI?!?!?!?!??*
That's right. We have a functional discord bot now. It might not do much yet, but it has big plans to take of the ~~universe~~ discord server and make things much easier for you to manage your server! 

## Discord updated itself too.
If you haven't noticed, today discord announced they're letting everyone into the hypesquad! You can take the wumpus test and your category will be given to you by the magic sorting hat. ***HYPESQUAD BALANCE FTW!!!!***
