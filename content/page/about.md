---
title: About us
subtitle: Why we are doing what we do
comments: false
---

### Our name is DiscordLists, here's who we are:

DiscordLists, a new website for listing your discord server or bot, but everyone knows that. Here's what makes us special and different then other lists:

We listen; We listen to the community and take your feedback.<br>
We're passionate; We're passionate about our project and want this to happen.<br>
We're a part of the community; We don't just run a community, we interact with it.


### Our History:

We all come from different backgrounds, from all around the world. We have one thing in common though, software and web development, if you'd like to join our team, you can pop us an email over at [mail@example.com](#)